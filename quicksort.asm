# This MIPS program sorts a set of numbers using the quicksort algorithm

.data
title:		.asciiz "Welcome to QuickSort\n" #used to display name of program
sorted:		.asciiz "\nThe sorted array is: " #used to show sorted list
reinit:		.asciiz "\nThe array is re-initialized" #used to show list has been cleared
newline: 	.asciiz "\n"
buffer: 	.space 512	# List in text form (overwritten once list is sorted)
list:		.space 2048	# List in integer form (sorted in place)
size:		.byte 0		# Size of list

	.text
	.globl main

main:	# all subroutines you create must come below "main"
	la $a0, title
	jal pprompt 		# Print the title of the program using the pprompt subroutine

	forkinput:		# Essentially the main subroutine (Used so title is not printed over and over again)
	
	add $v0, $0, $0
	la $a0, buffer		
	jal ginp		# Get user's input, and place it (unsorted list) in the buffer using ginp subroutine 

	beq $v0, 113, quit	# If that input ends with the letter 'q', end the program using the quit subroutine

	beq $v0, 99, _clear	# If that input ends with the letter 'c', clear the list using the _clear subroutine 
	bne $v0, 115, forkinput # If the input doesn't end with 'q', 'c', or 's', restart the main routine without printing the title

	la $a0, sorted		# Print sorted array introductory text
	jal pprompt

	la $a0, list		# Convert the user's input into an array of integers stored into the 'list' address
	la $a1, buffer
	lb $a2, size
	jal clean

	sb $v0, size		# Update the size of the array according to how many integers the user added in the input.

	la $a0, list 
	addi $a1, $0, 0
	lb $a2,size  
	subi $a2, $a2, 1
	jal _quicksort 		# Run the quicksort algorithm using the list. second argument is zero (lowest index) and the third is one less than the size of the list (highest index)

	la $a0, list		# Convert the list of integers to a string
	la $a1, buffer
	lb $a2, size
	jal inttostring

	la $a0, buffer  	# Print that string list
	jal pprompt

	la $a0, newline		# Start new line
	jal pprompt

	j forkinput		# Once this is done, restart main function without printing title

	_clear:			# If end of input is 'c'
		la $a0, list
		sb $0, size	# Set the size back to zero
		jal clear	# Run the clear method

		la $a0, reinit
		jal pprompt 	# Print reinitialization method

		la $a0, newline
		jal pprompt	# And newline

		j forkinput	# restart main function without printing title

pprompt:
	lui $t0, 0xffff		# load the input address
	add $t3, $a0, $0	# load the address of the output string

	phelper:
		lb $t2, ($t3)	# Load each byte of the string
		lw $t1, 8($t0)
		andi, $t1, $t1, 0x0001	# Wait for new data to be loaded
		beqz, $t1, phelper	# If no data is loaded, keep waiting
		sw, $t2, 12($t0)	# Else, output loaded byte
		addi $t3, $t3, 1	# Increase storing address by one (so every byte is printed)
		bne, $t2, 0, phelper	# If the end of the file is not reached, repeat this
		add $t0, $0, $0		# If end of the file is reached, return all values to their original state
		add $t1, $0, $0
		add $t2, $0, $0
		add $t3, $0, $0
		jr $ra			# and jump to return address

ginp:
	lui $t0, 0xffff
	add $t3, $a0, $0		# load input address and address of the buffer

	ihelper:
		lw $t1, ($t0)	
		andi, $t1, $t1, 0x0001
		beqz, $t1, ihelper	# Wait for user to input data, if none is inputted, keep waiting
		lw, $t2, 4($t0)		# if a byte is inputted, load it
		bgt $t2, 57, alpha	# if it isn't a number or space, do not print it
		sw, $t2, 12($t0)	# otherwise, print it

		alpha:
		sb $t2, ($t3)		# Store the byte in buffer string
		addi $t3, $t3, 1	# Increase buffer position by 1 so all bytes are included

		ble $t2, 57, notalpha	# if it is a letter, load it to $v0. Used in main function to determine the next step
		add $v0, $0, $t2	

		notalpha:
		bne $t2, 10, ihelper	# If enter is pressed, 
		addi $t3, $t3, -1
		sb $0, ($t3)		# Set last byte in buffer to null char
		add $t0, $0, $0
		add $t1, $0, $0		# Return all values to zero
		add $t2, $0, $0
		add $t3, $0, $0
		jr $ra			# and jump to return address

clean:					# this converts the input string to a list of integers
	add $t4, $a2, $0
	mul $t4, $t4, 4			# Since the list stores a byte every 4 positions, get the first unoccupied index by multiplying its size by 4
	addi $t0, $a0, -4		# load the address of the integer array
	add $t0, $t0, $t4		# and go to last occupied position

	addi $t1, $a1, -1		# load one less than the address of the buffer array

	add $t4, $a2, $0		# once more, load array size

	cleanhelper:
		addi $t1, $t1, 1	# Go through every byte in buffer (index was originally -1, but it is increased by 1, so we start iterating at index 0)
		lb $t2, ($t1)
		beq $t2, 32, cleanhelper	#ignore spaces
		ble $t2, 57, addbyte		# if it is an integer, add it
		add $v0, $0, $t4		# otherwise, we are done
						# we save the new size 
		jr $ra				# and go back to return address

		addbyte:
			addi $t0, $t0, 4	# go up one position in the array
			addi $t1, $t1, 1	# and in buffer string
			addi $t4, $t4, 1	# increase size by 1
			lb $t3, ($t1)		# check if adjacent byte is also a number
			bge $t3, 48, double	# if it is, go to double routine
			subi $t2, $t2, 48
			sb $t2, ($t0)		#Otherwise, simply store that number to the current array position
			j cleanhelper		# and repeat the routine until no more numbers are left

			double:				#if adjacent byte is a number,
				subi $t2, $t2, 48
				mul $t2, $t2, 10
				subi $t3, $t3, 48
				add $t2, $t2, $t3
				sb $t2, ($t0)		#store the sum of the quotient of the first byte and 10 and the second byte
				j cleanhelper		# and repeat the routine until no more numbers are left

clear:		#from _clear...
	addi $t0, $a0, -4

	clearhelper:
		addi $t0, $t0, 4	# Since $t0 was originally -4, we start at $t0=4
		lb $t1, ($t0)
		sb $0, ($t0)		# Go through every non-zero element of the integer list and make it zero
		bne $t1, $0, clearhelper # If the element already equals zero, we know we are done...
		jr $ra			# So we jump back to return address

inttostring:
	add $t0, $a0, $0		# Load list of ints and buffer string (load the addresses of their first respective positions)
	add $t1, $a1, $0
	add $t3, $0, $a2		# load size of array

	inthelper:
		beq $t3, 0, strung	# while we have not yet iterated the number of times equal to the size of the array...
		subi $t3, $t3, 1
		lb $t2, ($t0)		# load the array byte at the current position 
		bge $t2, 10, doublechar	# if it is greater than 10, add it as a double character
		addi $t2, $t2, 48	# otherwise, add it the the buffer string
		sb $t2, ($t1)
		add $t1, $t1, 1
		add $t2, $0, 32		# and add a space next to it
		sb $t2, ($t1)
		add $t1, $t1, 1
		add $t0, $t0, 4		# move up one position in the array
		j inthelper

	doublechar:
		add $t4, $0, 10	
		div $t2, $t4
		mflo $t2
		addi $t2, $t2, 48
		sb $t2, ($t1)		# add the quotient of this number and 10 to the buffer string
		add $t1, $t1, 1
		mfhi $t2
		addi $t2, $t2, 48
		sb $t2, ($t1)		# followed by the remainder of the same operation
		add $t1, $t1, 1
		add $t2, $0, 32		# followed by a space
		sb $t2, ($t1)
		add $t1, $t1, 1
		add $t0, $t0, 4		# move up one position in the array
		j inthelper
	strung:				# once we have iterated the number of times equal to the size o
		add $t2, $0, $0
		sb $t2, ($t1)		# make sure the buffer ends with a null character
		jr $ra			# and go back to the return addres


_quicksort:
	subi $sp, $sp, 4		# Save the return address to the stack
	sw $ra, ($sp)
	jal quicksort			# Start sorting. Save this address as new return address.
	lw $ra, ($sp)			# Once sorting is complete. Reset the return address to its original value 
	addi $sp, $sp, 4		# restore the stack to its original state
	add $t4, $0, $0			#restore all values to zero
	add $t5, $0, $0
	add $t6, $0, $0
	jr $ra				# Go to position directly after call

quicksort:
	add $t0, $a1, $a2
	div $t0, $t0, 2			# Take the middle value of the list as pivot
	mul $t0, $t0, 4
	add $t0, $t0, $a0		# List contains values every 4 bits, so save address of middle of list
	lw $t4, ($t0)			# save the value of the pivot
	add $t5, $0, $a1 
	add $t6, $0, $a2 		# save the value of highest and lowest indices

		blt $t6, $t5, crossed	# we count backwards from highest index, forward from lowest index until they reach eachother
		
		left:
			mul $t0, $t5, 4		# we start by counting forward from lowest index (left)
			add $t0, $t0, $a0	
			lw $t0, ($t0)		# load the value at the current index
			bge $t0, $t4, right	# if this value is greater than the pivot, 
			bge $t5, $a2, right	# or end of the list is reached, we check the bit at highest position
		 	addi $t5, $t5, 1	# otherwise, increase the position by 1 and repeat
		 	j left

		right:
			mul $t0, $t6, 4		# load the bit in the highest index (right)
			add $t0, $t0, $a0
			lw $t0, ($t0)
			bge $t4, $t0, switch	#if it is less than the pivot or,
			bge, $a1, $t6, switch	# or beginning of the list is reached, we switch the values of the lowest (left) and highest (right) elements
			subi $t6, $t6, 1	# otherwise, decrease the position by 1 and 
			j right

		switch:
			blt, $t6, $t5, crossed	#if the left pointer and right pointer have met, don't do anything
			mul $t0, $t5, 4
			add $t0, $t0, $a0
			lw $t1, ($t0)			#we load the value of the right element
			mul $t2, $t6, 4
			add $t2, $t2, $a0
			lw $t3, ($t2)			# we load the value of the left element
			sw $t1, ($t2)			# we store the value of the right element in the position of the left
			sw $t3, ($t0)			# store the value of the left element in the position of the right
			addi $t5, $t5, 1
			subi $t6, $t6, 1
			
	crossed:
		bge $a1, $t6, sortright				# if there is one or more bit in the left portion of the list...
		subi $sp, $sp, 12				# make room in the stack
		sw $a2, ($sp)					# store the byte of the highest index 
		sw $t5, 4($sp)					# store the position of the left byte
		sw $ra, 8($sp)					#store the current return address
		add $a2, $t6, $0
		jal quicksort					#rerun the previous quicksort routine with the highest index set at the position of the right byte
		lw $a2, ($sp)					#this is done recursively (over and over again) until the left bit reaches the beginning of the list
		lw $t5, 4($sp)
		lw $ra, 8($sp)
		addi $sp, $sp, 12
	
	sortright:						# if there is more than one bit in the right portion...
		bge $t5, $a2, sortedd
		subi $sp, $sp, 4				# store the return address in the stack
		sw $ra, ($sp)
		add $a1, $t5, $0				# rerun the previous quicksort routine with the lowest index set at the position of the left byte
		jal quicksort
		lw $ra, ($sp)
		addi $sp, $sp, 4
	sortedd:					#otherwise, the list is sorted, and we exit
	jr $ra

quit:		#Exits the program by loading the appropriate syscall
	li $v0, 10
	syscall

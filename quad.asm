	.data

prompt1: 	.asciiz "Input value a:"
prompt2: 	.asciiz "Input value b:"
prompt3: 	.asciiz "Input value c:"
answer1: 	.asciiz "There is no solution."
answer2: 	.asciiz "The solution set is ( "
answer3: 	.asciiz " "
answer4: 	.asciiz ")"


	.text
	.globl main
	main:
		li $v0, 4
		la $a0, prompt1
		syscall
		li $v0, 5
		syscall
		move $s0, $v0
		li $v0, 4
		la $a0, prompt2
		syscall
		li $v0, 5
		syscall
		move $s1, $v0
		li $v0, 4
		la $a0, prompt3
		syscall
		li $v0, 5
		syscall			# Prompt user for values of a,b, and c
		move $s2, $v0		# Store those values in $s0, $s1, and $s2 respectively
		div $s0, $s1
		mfhi $s3		# Find the value of a mod b	
		li $t0, 1		# $t0 will take the value of every number between 1 and c. Its initial value is 1, and it increases by 1 at each step.
		li $t3, 0		# $t3 is 1 if answers have been found, 0 if no answers have been found yet	
	rec:
		bgt $t0, $s2, end	# If $t0 is greater than c, go to 'end'
		div $t0, $s1
		mfhi $t1		# Otherwise, find the value of $t0 mod b
		mult $t1, $t1		# Square this value
		mflo $t2		
		div $t2, $s1
		mfhi $t2		# Find the value of this square mod b
		beq $t2, $s3, addnum	# If thos square is equivalent to a mod b, add it to answers by jumping to 'addnum'
		add $t0, $t0, 1		# Increase $t0 by 1 and see if this new value is an answer
		j rec
	
	addnum:
		beq $t3, 0, addyes	# If this was the first answer found, add intro text
		li $v0, 1
		la $a0, ($t0)
		syscall			# Print answer followed by a space
		li $v0, 4
		la $a0, answer3
		syscall
		add $t0, $t0, 1		# Increase $t0 by 1 and see if this new value is an answer
		j rec
	addyes:
		li $t3, 1		# Answer was found, so set $t3 to 1
		li $v0, 4
		la $a0, answer2
		syscall			# Print intro text, and jump back to 'addnum' (since $t3 is now 1, it will not jump here again)
		j addnum
	addpar:
		li $v0, 4		# Add closing parentheses and end the program
		la $a0, answer4
		syscall
		li $v0, 10
		syscall	
	end:
		beq $t3, 1, addpar	# If answers were found, close the parentheses
		li $v0, 4
		la $a0, answer1
		syscall
		li $v0, 10
		syscall			# Otherwise, say no answers were found and end the program
		
		

		
